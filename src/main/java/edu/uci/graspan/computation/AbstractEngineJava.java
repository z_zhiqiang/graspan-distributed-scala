package edu.uci.graspan.computation;

import java.io.Serializable;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;

import com.google.common.base.Optional;

import edu.uci.graspan.datastructure.AdjacencyList;
import edu.uci.graspan.datastructure.Array2DAL;
import edu.uci.graspan.datastructure.Edge;
import edu.uci.graspan.datastructure.HashSetAL;
import edu.uci.graspan.datastructure.SortedArrayAL;
import edu.uci.graspan.datastructure.VLTuple;
import edu.uci.graspan.grammar.GrammarChecker;
import scala.Tuple2;

public abstract class AbstractEngineJava implements Serializable {
	
	public static final byte HASHSETAL = 0, SORTARRAYAL = 1, ARRAY2DAL = 2;
	

//	public abstract JavaRDD<Edge> run(JavaSparkContext jsc, final Broadcast<GrammarChecker> gChecker_broadcast, JavaRDD<Edge> graph_input, final int number_partitions);

	public static void printOutPartitioner(JavaPairRDD rdd, String point){
		if(rdd.partitioner().isPresent()){
			System.out.println(point + ":\t" + rdd.partitioner().get());
		}
		else{
			System.out.println(point + ":\t" + false);
		}
	}
	
	
	
	protected static class EdgeListConstructor implements Function<Iterable<VLTuple>, AdjacencyList> {
		
		final byte mode_ds;
		
		final boolean sorted_flag;
		
		final byte size_labels;
		
//		public EdgeListConstructor(byte mode){
//			mode_ds = mode;
//			sorted_flag = true;
//			size_labels = 0;
//		}
//		
//		public EdgeListConstructor(byte mode, boolean sorted_f){
//			mode_ds = mode;
//			sorted_flag = sorted_f;
//			size_labels = 0;
//		}
		
		public EdgeListConstructor(byte mode, boolean sorted_f, byte s_labels){
			mode_ds = mode;
			sorted_flag = sorted_f;
			size_labels = s_labels;
		}

		@Override
		public AdjacencyList call(Iterable<VLTuple> v) throws Exception {
			switch (mode_ds){
			case HASHSETAL:
				return new HashSetAL(v);
			case SORTARRAYAL:
				return new SortedArrayAL(v, sorted_flag);
			case ARRAY2DAL:
				return new Array2DAL(size_labels, v, sorted_flag);
			default:
				throw new RuntimeException("Wrong mode!!!");
			}
		}
	}
	
	protected static class EdgeFlattener implements FlatMapFunction<Tuple2<Integer, AdjacencyList>, Edge> {

		@Override
		public Iterable<Edge> call(Tuple2<Integer, AdjacencyList> t) throws Exception {
			return t._2().flattenEdges(t._1());
		}
		
	}
	
	
	protected static class DstPairFlattener implements PairFlatMapFunction<Tuple2<Integer, AdjacencyList>, Integer, VLTuple> {

		@Override
		public Iterable<Tuple2<Integer, VLTuple>> call(Tuple2<Integer, AdjacencyList> t) throws Exception {
			return t._2().flattenDstPairs(t._1());
		}
		
	}
	
	
	protected static class EdgeListReducer implements Function2<AdjacencyList, AdjacencyList, AdjacencyList> {

		@Override
		public AdjacencyList call(AdjacencyList v1, AdjacencyList v2) throws Exception {
			return v1.merge(v2);
		}
		
	}
	
	
	protected static JavaPairRDD<Integer, AdjacencyList> generateDeltaRDD(
			JavaPairRDD<Integer, Tuple2<Optional<AdjacencyList>, Optional<AdjacencyList>>> joined_old_new) {
		return joined_old_new.mapValues(new Function<Tuple2<Optional<AdjacencyList>, Optional<AdjacencyList>>, AdjacencyList>(){

			@Override
			public AdjacencyList call(Tuple2<Optional<AdjacencyList>, Optional<AdjacencyList>> v)
					throws Exception {
				if(!v._1().isPresent()){
					return v._2().get();
				}
				else if(!v._2().isPresent()){
					return null;
				}
				else{
					return v._2().get().subtract(v._1().get());
				}
			}
			
		}).filter(new Function<Tuple2<Integer, AdjacencyList>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AdjacencyList> v) throws Exception {
				return v._2() != null;
			}
			
		});
	}
	
	
	protected static JavaPairRDD<Integer, AdjacencyList> generateFullRDD(
			JavaPairRDD<Integer, Tuple2<Optional<AdjacencyList>, Optional<AdjacencyList>>> joined_old_new) {
		return joined_old_new.mapValues(new Function<Tuple2<Optional<AdjacencyList>, Optional<AdjacencyList>>, AdjacencyList>(){

			@Override
			public AdjacencyList call(Tuple2<Optional<AdjacencyList>, Optional<AdjacencyList>> v)
					throws Exception {
				if(!v._1().isPresent()){
					return v._2().get();
				}
				else if(!v._2().isPresent()){
					return v._1().get();
				}
				else{
					return v._1().get().merge(v._2().get());
				}
			}
			
		});
	}

	
	protected static JavaPairRDD<Integer, AdjacencyList> addEdges1_list(JavaPairRDD<Integer, AdjacencyList> edges_list,
			final Broadcast<GrammarChecker> gChecker_broadcast) {
		return edges_list.mapValues(new Function<AdjacencyList, AdjacencyList>(){

			@Override
			public AdjacencyList call(AdjacencyList v1) throws Exception {
				return v1.addEdgesL1(gChecker_broadcast);
			}
			
		}).filter(new Function<Tuple2<Integer, AdjacencyList>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AdjacencyList> v) throws Exception {
				// TODO Auto-generated method stub
				return v._2() != null;
			}
			
		});
	}
	
	
	protected static JavaPairRDD<Integer, AdjacencyList> addEdges2_list_el(JavaPairRDD<Integer, VLTuple> dst_paired,
			JavaPairRDD<Integer, AdjacencyList> edges_list, final Broadcast<GrammarChecker> gChecker_broadcast) {
		JavaPairRDD<Integer, Tuple2<VLTuple, AdjacencyList>> joined_rdd = dst_paired.join(edges_list);
//		printOutPartitioner(joined_rdd, "joined_rdd");
		
		JavaPairRDD<Integer, AdjacencyList> edges_added_2_list = joined_rdd.values().mapToPair(new PairFunction<Tuple2<VLTuple, AdjacencyList>, Integer, AdjacencyList>(){

			@Override
			public Tuple2<Integer, AdjacencyList> call(Tuple2<VLTuple, AdjacencyList> t)
					throws Exception {
				AdjacencyList list = t._2().addEdgesL2(gChecker_broadcast, t._1().getLabel());
				
				if(list != null){
					return new Tuple2(t._1().getVId(), list);
				}

				return null;
			}

			
		}).filter(new Function<Tuple2<Integer, AdjacencyList>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AdjacencyList> v) throws Exception {
				return v != null;
			}
			
		}).reduceByKey(new EdgeListReducer());
		
		return edges_added_2_list;
	}
	
	protected static JavaPairRDD<Integer, AdjacencyList> addEdges2_list_ll(JavaPairRDD<Integer, AdjacencyList> dst_list,
			JavaPairRDD<Integer, AdjacencyList> edges_list, final Broadcast<GrammarChecker> gChecker_broadcast) {
		JavaPairRDD<Integer, Tuple2<AdjacencyList, AdjacencyList>> joined_rdd = dst_list.join(edges_list);
//		printOutPartitioner(joined_rdd, "joined_rdd");
		
		JavaPairRDD<Integer, AdjacencyList> edges_added_2_list = joined_rdd.values().flatMapToPair(new PairFlatMapFunction<Tuple2<AdjacencyList, AdjacencyList>, Integer, AdjacencyList>(){

			@Override
			public Iterable<Tuple2<Integer, AdjacencyList>> call(Tuple2<AdjacencyList, AdjacencyList> t)
					throws Exception {
				// TODO Auto-generated method stub
				return t._1().addEdgesL2_List(gChecker_broadcast, t._2());
			}

			
		}).filter(new Function<Tuple2<Integer, AdjacencyList>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AdjacencyList> v) throws Exception {
				return v != null;
			}
			
		}).reduceByKey(new EdgeListReducer());
		
		return edges_added_2_list;
	}
	
	
}
