package edu.uci.graspan.preprocess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;

import edu.uci.graspan.datastructure.Edge;
import edu.uci.graspan.grammar.GrammarChecker;

public class Preprocessor {
	
	public static JavaRDD<Edge> preprocess(JavaSparkContext jsc, final Broadcast<GrammarChecker> gChecker_broadcast, final String input_graph, final int num_partitions){
		//check the validity of edge labels
		JavaRDD<Edge> checked_graph = checkInput(jsc, gChecker_broadcast, input_graph, num_partitions).cache();

		//add self-loop edges according to epsilon-grammar
		JavaRDD<Edge> graph = addLoopEdges(gChecker_broadcast, checked_graph, num_partitions);
		
		return graph;
		
	}

	private static JavaRDD<Edge> addLoopEdges(final Broadcast<GrammarChecker> gChecker_broadcast,
			JavaRDD<Edge> checked_graph, int num_partitions) {
		//get all the unique vertices
		JavaRDD<Integer> vertexRdd = checked_graph.flatMap(new FlatMapFunction<Edge, Integer>(){

			@Override
			public Iterable<Integer> call(Edge t) throws Exception {
				List<Integer> list = new ArrayList<Integer>(2);
				list.add(t.getSrcId());
				list.add(t.getDstId());
				return list;
			}
			
		}).distinct(num_partitions);
		
		JavaRDD<Edge> loopEdges = vertexRdd.flatMap(new FlatMapFunction<Integer, Edge>(){

			@Override
			public Iterable<Edge> call(Integer t) throws Exception {
				int size = gChecker_broadcast.getValue().geteRules().size();
				List<Edge> list = new ArrayList<Edge>(size);
				for(byte label: gChecker_broadcast.getValue().geteRules()){
					Edge loopEdge = new Edge(t, t, label);
					list.add(loopEdge);
				}
				return list;
			}
			
		});
		
		//union the newly added self-loop edges into the graph
		JavaRDD<Edge> graph = checked_graph.union(loopEdges).distinct();
		
		return graph;
	}

	private static JavaRDD<Edge> checkInput(JavaSparkContext jsc, final Broadcast<GrammarChecker> gChecker_broadcast,
			final String input_graph, final int num_partitions) {
		//initiate an accumulator for invalid input
		final Accumulator<Integer> invalidInputAccumulator = jsc.intAccumulator(0); 
				
		JavaRDD<String> input_raw = jsc.textFile(input_graph, num_partitions);
		JavaRDD<String> graph = input_raw.filter(new Function<String, Boolean>(){

			@Override
			public Boolean call(String v1) throws Exception {
				// TODO Auto-generated method stub
				return !(v1.trim().isEmpty());
			}
			
		});
		
		JavaRDD<Edge> checked_graph = graph.map(new Function<String, Edge>(){

			@Override
			public Edge call(String line) throws Exception {
				// TODO Auto-generated method stub
				String[] tokens = line.trim().split("\\s+");
				if(isInvalid(tokens, gChecker_broadcast)){
					invalidInputAccumulator.add(1);
					System.err.println("Invalid edge:\t" + line);
					return null;
				}
				
				return new Edge(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]), gChecker_broadcast.getValue().getValue(tokens[2]));
			}

			private boolean isInvalid(String[] tokens, Broadcast<GrammarChecker> gChecker_broadcast) {
				// TODO Auto-generated method stub
				if(tokens.length != 3 || !gChecker_broadcast.getValue().containsLabel(tokens[2])){
					return true;
				}
				return false;
			}
		});
		
		if(invalidInputAccumulator.value() != 0){
			System.err.println("Application stopped due to the invalid edge label!!!");
			jsc.cancelAllJobs();
			jsc.stop();
			System.exit(0);
		}
		
		return checked_graph;
	}

}
