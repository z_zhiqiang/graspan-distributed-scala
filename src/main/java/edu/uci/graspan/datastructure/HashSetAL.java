package edu.uci.graspan.datastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.spark.broadcast.Broadcast;

import edu.uci.graspan.grammar.GrammarChecker;
import scala.Tuple2;

public class HashSetAL extends AdjacencyList {

	private final HashSet<VLTuple> edges_list;
	
	public HashSetAL(){
		this.edges_list = new HashSet<VLTuple>();
	}
	
	public HashSetAL(Iterable<VLTuple> l){
		this.edges_list = new HashSet<VLTuple>();
		for(VLTuple tuple: l){
			this.edges_list.add(tuple);
		}
	}
	

//	public HashSet<VLTuple> getEdges_list() {
//		return edges_list;
//	}


	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return this.edges_list.isEmpty();
	}


//	@Override
//	public AdjacencyList addEdgesL1(Broadcast<GrammarChecker> gChecker_broadcast) {
//		return addEdgesL1(gChecker_broadcast.getValue().getsRules());
//	}
//
//	
//	@Override
//	public AdjacencyList addEdgesL2(Broadcast<GrammarChecker> gChecker_broadcast, VLTuple vlTuple) {
//		Map<Byte, Byte> map = gChecker_broadcast.getValue().getL2Map(vlTuple.getLabel());
//		if(map != null){
//			return addEdgesL1(map);
//		}
//
//		return null;
//	}

	
	public AdjacencyList addEdgesL1(Map<Byte, Byte> map) {
		HashSetAL nl = new HashSetAL();
		for(VLTuple tuple: this.edges_list){
			byte rLabel = GrammarChecker.checkL1Rules(map, tuple.getLabel());
			if(rLabel != -1){
				VLTuple new_tuple = new VLTuple(tuple.getVId(), rLabel);
				nl.edges_list.add(new_tuple);
			}
		}
		
		if(!nl.isEmpty()){
			return nl;
		}
		
		return null;
	}

	
	@Override
	public AdjacencyList merge(AdjacencyList l2) {
		HashSetAL nl = new HashSetAL();
		nl.edges_list.addAll(this.edges_list);
		nl.edges_list.addAll(((HashSetAL)l2).edges_list);
		return nl;
	}


	@Override
	public AdjacencyList subtract(AdjacencyList l2) {
		HashSetAL nl = new HashSetAL();
		nl.edges_list.addAll(this.edges_list);
		nl.edges_list.removeAll(((HashSetAL)l2).edges_list);
		
		if(!nl.isEmpty()){
			return nl;
		}
		
		return null;
	}


	@Override
	public Iterable<Tuple2<Integer, VLTuple>> flattenDstPairs(int srcId) {
		List<Tuple2<Integer, VLTuple>> list = new ArrayList<Tuple2<Integer, VLTuple>>(this.edges_list.size());
		for(VLTuple tuple: this.edges_list){
			Tuple2 pair = new Tuple2(tuple.getVId(), new VLTuple(srcId, tuple.getLabel()));
			list.add(pair);
		}
		return list;
	}


	@Override
	public Iterable<Edge> flattenEdges(int srcId) {
		List<Edge> list = new ArrayList<Edge>(this.edges_list.size());
		for(VLTuple tuple: this.edges_list){
			list.add(new Edge(srcId, tuple.getVId(), tuple.getLabel()));
		}
		return list;
	}

	@Override
	public Iterable<Tuple2<Integer, AdjacencyList>> addEdgesL2_List(Broadcast<GrammarChecker> gChecker_broadcast,
			AdjacencyList src_list) {
		// TODO Auto-generated method stub
		List<Tuple2<Integer, AdjacencyList>> list = new ArrayList<Tuple2<Integer, AdjacencyList>>();
		for(VLTuple tuple: this.edges_list){
			AdjacencyList l = src_list.addEdgesL2(gChecker_broadcast, tuple.getLabel());
			if(l != null){
				list.add(new Tuple2(tuple.getVId(), l));
			}
		}
		return list;
	}


	@Override
	public Iterable<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>> flattenEdgeList(int srcId) {
		List<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>> list = new ArrayList<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>>(this.edges_list.size());
		for(VLTuple tuple: this.edges_list){
			list.add(new Tuple2(new SrcKey(srcId, tuple.getVId()), new Tuple2(tuple.getLabel(), this)));
		}
		return list;
	}

	@Override
	public String toString() {
		return this.edges_list.toString() + "@" + System.identityHashCode(this);
	}

	@Override
	public Iterable<Tuple2<SrcKey, Byte>> flattenSrcEdges(int srcId) {
		List<Tuple2<SrcKey, Byte>> list = new ArrayList<Tuple2<SrcKey, Byte>>(this.edges_list.size());
		for(VLTuple tuple: this.edges_list){
			list.add(new Tuple2(new SrcKey(srcId, tuple.getVId()), tuple.getLabel()));
		}
		return list;
	}

	

}
