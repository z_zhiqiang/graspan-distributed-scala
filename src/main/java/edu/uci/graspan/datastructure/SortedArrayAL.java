package edu.uci.graspan.datastructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.spark.broadcast.Broadcast;

import edu.uci.graspan.grammar.GrammarChecker;
import scala.Tuple2;

public class SortedArrayAL extends AdjacencyList {

	private final int[] ids;
	
	private final byte[] labels;
	
	private int size;
	
	private final int capacity;
	
	
	private final boolean sorted; 
	
	
	public SortedArrayAL(int c){
		this.capacity = c;
		this.ids = new int[this.capacity];
		this.labels = new byte[this.capacity];
		this.size = 0;
		this.sorted = true;
	}
	
	
	public SortedArrayAL(int c, boolean sorted){
		this.capacity = c;
		this.ids = new int[this.capacity];
		this.labels = new byte[this.capacity];
		this.size = 0;
		this.sorted = sorted;
	} 
	
	public SortedArrayAL(Iterable<VLTuple> l, boolean sorted_flag){
		this.sorted = sorted_flag;
		if(this.sorted){//sorted array for src-list
			List<VLTuple> list = new ArrayList<VLTuple>();
			for(VLTuple tuple: l){
				list.add(tuple);
			}
			Collections.sort(list);
			
			
//		//remove duplicates
//		for(int i = 0; i < list.size() - 1;){
//			if(list.get(i) == list.get(i + 1)){
//				list.remove(i);
//			}
//			else{
//				i++;
//			}
//		}
			
			this.capacity = list.size();
			this.ids = new int[this.capacity];
			this.labels = new byte[this.capacity];
			this.size = list.size();
			
			int i = 0;
			for(VLTuple tuple: list){
				this.ids[i] = tuple.getVId();
				this.labels[i++] = tuple.getLabel();
			}
			assert(i == this.size);
		}
		else{//unsorted array for dst-list
			int s = 0;
			for(VLTuple tuple: l){
				s++;
			}
			
			this.capacity = s;
			this.ids = new int[this.capacity];
			this.labels = new byte[this.capacity];
			this.size = s;
			
			int i = 0;
			for(VLTuple tuple: l){
				this.ids[i] = tuple.getVId();
				this.labels[i++] = tuple.getLabel();
			}
			assert(i == this.size);
			
		}
	}
	
	
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size == 0;
	}

//	@Override
//	public AdjacencyList addEdgesL1(Broadcast<GrammarChecker> gChecker_broadcast) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public AdjacencyList addEdgesL2(Broadcast<GrammarChecker> gChecker_broadcast, VLTuple vlTuple) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public AdjacencyList merge(AdjacencyList l2) {
		// TODO Auto-generated method stub
		SortedArrayAL l2_cast = (SortedArrayAL) l2;
		assert(this.sorted == l2_cast.sorted);
		SortedArrayAL nl = new SortedArrayAL(this.size + l2_cast.size, this.sorted);
		
		if(this.sorted){
			int i = 0, j = 0;
			
			while(i < this.size && j < l2_cast.size){
				int id = this.ids[i];
				byte label = this.labels[i];
				int another_id = l2_cast.ids[j];
				byte another_label = l2_cast.labels[j];
				
				int cmp = compare(id, label, another_id, another_label);
				if(cmp < 0){
					nl.addIdLabel(id, label);
					i++;
				}
				else{
					nl.addIdLabel(another_id, another_label);
					j++;
					if(cmp == 0){
						i++;
					}
				}
			}
			
			if(i < this.size){
				System.arraycopy(this.ids, i, nl.ids, nl.size, this.size - i);
				System.arraycopy(this.labels, i, nl.labels, nl.size, this.size - i);
				nl.size += this.size - i;
			}
			
			if(j < l2_cast.size){
				System.arraycopy(l2_cast.ids, j, nl.ids, nl.size, l2_cast.size - j);
				System.arraycopy(l2_cast.labels, j, nl.labels, nl.size, l2_cast.size - j);
				nl.size += l2_cast.size - j;
			}
			
		}
		else{
			System.arraycopy(this.ids, 0, nl.ids, 0, this.size);
			System.arraycopy(this.labels, 0, nl.labels, 0, this.size);
			nl.size += this.size;
			
			System.arraycopy(l2_cast.ids, 0, nl.ids, nl.size, l2_cast.size);
			System.arraycopy(l2_cast.labels, 0, nl.labels, nl.size, l2_cast.size);
			nl.size += l2_cast.size;
		}
		
		return nl;
	}

	private static int compare(int id, byte label, int another_id, byte another_label) {
		if(id != another_id){
			return id - another_id;
		}
		return label - another_label;
	}

	@Override
	public AdjacencyList subtract(AdjacencyList l2) {
		// TODO Auto-generated method stub
		SortedArrayAL l2_cast = (SortedArrayAL) l2;
		SortedArrayAL nl = new SortedArrayAL(this.size);
		int i = 0, j = 0;
		
		while(i < this.size && j < l2_cast.size){
			int id = this.ids[i];
			byte label = this.labels[i];
			int another_id = l2_cast.ids[j];
			byte another_label = l2_cast.labels[j];
			
			int cmp = compare(id, label, another_id, another_label);
			if(cmp < 0){
				nl.addIdLabel(id, label);
				i++;
			}
			else{
				j++;
				if(cmp == 0){
					i++;
				}
			}
		}
		
		if(i < this.size){
			System.arraycopy(this.ids, i, nl.ids, nl.size, this.size - i);
			System.arraycopy(this.labels, i, nl.labels, nl.size, this.size - i);
			nl.size += this.size - i;
		}
		
		if(!nl.isEmpty()){
			return nl;
		}
		
		return null;
	}

	@Override
	public Iterable<Tuple2<Integer, VLTuple>> flattenDstPairs(int srcId) {
		List<Tuple2<Integer, VLTuple>> list = new ArrayList<Tuple2<Integer, VLTuple>>(this.size);
		for(int i = 0; i < this.size; i++){
			Tuple2 pair = new Tuple2(this.ids[i], new VLTuple(srcId, this.labels[i]));
			list.add(pair);
		}
		return list;
	}

	@Override
	public Iterable<Edge> flattenEdges(int srcId) {
		List<Edge> list = new ArrayList<Edge>(this.size);
		for(int i = 0; i < this.size; i++){
			list.add(new Edge(srcId, this.ids[i], this.labels[i]));
		}
		return list;
	}


	@Override
	public AdjacencyList addEdgesL1(Map<Byte, Byte> map) {
		SortedArrayAL nl = new SortedArrayAL(this.size);
		for(int i = 0; i < this.size; i++){
			byte rLabel = GrammarChecker.checkL1Rules(map, this.labels[i]);
			if(rLabel != -1){
				nl.addIdLabel(this.ids[i], rLabel);
			}
		}
		
		if(!nl.isEmpty()){
			return nl;
		}
		
		return null;
	}
	
	private void addIdLabel(int id, byte label){
		this.ids[this.size] = id;
		this.labels[this.size++] = label;
	}
	
	public int getSize(){
		return this.size;
	}

	@Override
	public Iterable<Tuple2<Integer, AdjacencyList>> addEdgesL2_List(Broadcast<GrammarChecker> gChecker_broadcast,
			AdjacencyList src_list) {
		List<Tuple2<Integer, AdjacencyList>> list = new ArrayList<Tuple2<Integer, AdjacencyList>>();
		for(int i = 0; i < this.size; i++){
			AdjacencyList l = src_list.addEdgesL2(gChecker_broadcast, this.labels[i]);
			if(l != null){
				list.add(new Tuple2(this.ids[i], l));
			}
		}
		return list;
	}


	@Override
	public Iterable<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>> flattenEdgeList(int srcId) {
		List<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>> list = new ArrayList<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>>(this.size);
		for(int i = 0; i < this.size; i++){
			list.add(new Tuple2(new SrcKey(srcId, this.ids[i]), new Tuple2(this.labels[i], this)));
		}
		return list;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		
		//content
		for(int i = 0; i < this.size - 1; i++){
			builder.append(this.ids[i]).append("-").append(this.labels[i]).append(", ");
		}
		builder.append(this.ids[this.size - 1]).append("-").append(this.labels[this.size - 1]);
		
		//address
		builder.append("]@").append(System.identityHashCode(this));
		
		return builder.toString();
	}


	@Override
	public Iterable<Tuple2<SrcKey, Byte>> flattenSrcEdges(int srcId) {
		List<Tuple2<SrcKey, Byte>> list = new ArrayList<Tuple2<SrcKey, Byte>>(this.size);
		for(int i = 0; i < this.size; i++){
			list.add(new Tuple2(new SrcKey(srcId, this.ids[i]), this.labels[i]));
		}
		return list;
	}




}
