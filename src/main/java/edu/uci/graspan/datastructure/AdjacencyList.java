package edu.uci.graspan.datastructure;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.spark.broadcast.Broadcast;

import edu.uci.graspan.grammar.GrammarChecker;
import scala.Tuple2;

public abstract class AdjacencyList implements Serializable {
	
	public abstract boolean isEmpty();
	
//	public abstract String toString();

	
	/**dstList * srcList => [(srcId, srcList)]
	 * @param gChecker_broadcast
	 * @param src_list
	 * @return
	 */
	public abstract Iterable<Tuple2<Integer, AdjacencyList>> addEdgesL2_List(Broadcast<GrammarChecker> gChecker_broadcast, AdjacencyList src_list);
	
	
	public abstract Iterable<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>> flattenEdgeList(int srcId);
	
	public abstract Iterable<Tuple2<SrcKey, Byte>> flattenSrcEdges(int srcId);
		
	
	public AdjacencyList addEdgesL2_ListList(Broadcast<GrammarChecker> gChecker_broadcast, byte label, AdjacencyList src_list){
		AdjacencyList list = addEdgesL2(gChecker_broadcast, label);
		
		if(list != null){
			AdjacencyList nlist = list.subtract(src_list);
			return nlist;
		}

		return list;
	}
	
	/**srcList => srcList
	 * @param gChecker_broadcast
	 * @return
	 */
	public AdjacencyList addEdgesL1(Broadcast<GrammarChecker> gChecker_broadcast) {
		return addEdgesL1(gChecker_broadcast.getValue().getsRules());
	}

	/**dstLabel * srcList => srcList
	 * @param gChecker_broadcast
	 * @param label
	 * @return
	 */
	public AdjacencyList addEdgesL2(Broadcast<GrammarChecker> gChecker_broadcast, byte label) {
		Map<Byte, Byte> map = gChecker_broadcast.getValue().getL2Map(label);
		if(map != null){
			return addEdgesL1(map);
		}
		
		return null;
	}

	public abstract AdjacencyList addEdgesL1(Map<Byte, Byte> getsRules);

	public abstract AdjacencyList merge(AdjacencyList l2);

	public abstract AdjacencyList subtract(AdjacencyList l2);

	public abstract Iterable<Tuple2<Integer, VLTuple>> flattenDstPairs(final int srcId);

	public abstract Iterable<Edge> flattenEdges(final int srcId);


//	public abstract Iterator<VLTuple> iterator();
	
}
