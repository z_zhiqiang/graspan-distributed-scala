package edu.uci.graspan.datastructure;

import java.io.Serializable;

import scala.Tuple2;

public class Edge implements Serializable {
	
	private final int srcId;
	private final int dstId;
	private final byte label;
	
	public Edge(int src, int dst, byte l){
		srcId = src;
		dstId = dst;
		label = l;
	}
	
	public Edge(Tuple2<Integer, Tuple2<Integer, Byte>> tuple){
		srcId = tuple._1();
		dstId = tuple._2()._1();
		label = tuple._2()._2();
	}
	
	public int getSrcId() {
		return srcId;
	}

	public int getDstId() {
		return dstId;
	}

	public byte getLabel() {
		return label;
	}
	
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append(this.srcId).append(",").append(this.dstId).append(",").append(this.label);
		return builder.toString();
	}
	
	public int hashCode(){
		int result = this.srcId;
//		result = 31 * result + this.srcId;
		result = 31 * result + this.dstId;
		result = 31 * result + this.label;
		return result;
	}
	
	public boolean equals(Object obj){
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass() != this.getClass()){
			return false;
		}
		
		Edge e = (Edge) obj;
		return this.srcId == e.srcId && this.dstId == e.dstId && this.label == e.label;
	}
	
	

}
