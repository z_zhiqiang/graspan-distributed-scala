package edu.uci.graspan.datastructure;

import java.io.Serializable;

public class VLTuple implements Comparable<VLTuple>, Serializable {
	
	private final int vId;
	
	private final byte label;
	
	
	public VLTuple(int dst, byte l){
		this.vId = dst;
		this.label = l;
	}


	public int getVId() {
		return vId;
	}


	public byte getLabel() {
		return label;
	}

	
	public int hashCode(){
		int result = 1;
		result = result * 31 + this.vId;
		result = result * 31 + this.label;
		return result;
	}
	
	public boolean equals(Object obj){
		if(this == obj){
			return true;
		}
		
		if(obj == null || obj.getClass() != this.getClass()){
			return false;
		}
		
		VLTuple t = (VLTuple) obj;
		return this.vId == t.vId && this.label == t.label;
	}


	@Override
	public int compareTo(VLTuple o) {
		// TODO Auto-generated method stub
		if(this.vId != o.vId){
			return this.vId - o.vId;
		}
		return this.label - o.label;
	}

	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append(this.vId).append("-").append(this.label);
		return builder.toString();
	}

	
}
