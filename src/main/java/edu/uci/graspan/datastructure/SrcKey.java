package edu.uci.graspan.datastructure;

public class SrcKey {
	private static int number_partitions;
	
	private final int partition_id;
	
	private final int src_id;
	
	
	public SrcKey(int p_id, int s_id){
		this.partition_id = p_id;
		this.src_id = s_id;
	}
	
	public static void set_num_partitions(int num_partitions){
		number_partitions = num_partitions;
	}
	
	public int hashCode(){
		return partition_id % number_partitions;
	}
	
	public boolean equals(Object obj){
		if(this == obj)
			return true;
		
		if(obj == null || obj.getClass() != this.getClass()){
			return false;
		}
		
		SrcKey key = (SrcKey) obj;
		return this.partition_id % number_partitions == key.partition_id % number_partitions && this.src_id == key.src_id;
	}

	public int getPartition_id() {
		return partition_id;
	}

	public int getSrc_id() {
		return src_id;
	}

	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		builder.append(this.partition_id).append(", ");
		builder.append(this.src_id);
		builder.append(")");
		return builder.toString();
	}
	

}
