package edu.uci.graspan.datastructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.spark.broadcast.Broadcast;

import edu.uci.graspan.grammar.GrammarChecker;
import scala.Tuple2;

public class Array2DAL extends AdjacencyList {
	
	private final static Array EMPTYARRAY = new Array(0);
	
	private final Array[] arrays;
	
	private final byte size_labels;
	
	private int size_edges;
	
	
	private final boolean sorted;
	
	public Array2DAL(byte s){
		this.size_labels = s;
		this.arrays = new Array[size_labels];
		this.size_edges = 0;
		this.sorted = true;
	}
	
	public Array2DAL(byte s, boolean sorted){
		this.size_labels = s;
		this.arrays = new Array[size_labels];
		this.size_edges = 0;
		this.sorted = sorted;
	}

	
	public Array2DAL(byte s, Iterable<VLTuple> l, boolean sorted_flag){
		this.size_labels = s;
		this.arrays = new Array[size_labels];
		this.sorted = sorted_flag;
		
		if(this.sorted){
			ArrayList<Integer>[] tmp = new ArrayList[this.size_labels];
			for(byte label = 0; label < tmp.length; label++){
				tmp[label] = new ArrayList<Integer>();
			}
			
			for(VLTuple tuple: l){
				tmp[tuple.getLabel()].add(tuple.getVId());
			}
			
			for(byte label = 0; label < tmp.length; label++){
				if(tmp[label].isEmpty()){
					finalPut(label, EMPTYARRAY);
				}
				else{
					Collections.sort(tmp[label]);
					finalPut(label, new Array(tmp[label]));
				}
			}
			
		}
		else{
			int[] tmp_size = new int[this.size_labels];
			for(VLTuple tuple: l){
				tmp_size[tuple.getLabel()]++;
			}
			
			for(byte label = 0; label < tmp_size.length; label++){
				if(tmp_size[label] == 0){
					finalPut(label, EMPTYARRAY);
				}
				else{
					finalPut(label, new Array(tmp_size[label]));
				}
			}
			
			for(VLTuple tuple: l){
				arrays[tuple.getLabel()].add(tuple.getVId());
				this.size_edges++;
			}
		}
		
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return this.size_edges == 0;
	}
	
	public boolean isSorted(){
		return this.sorted;
	}

	@Override
	public Iterable<Tuple2<Integer, AdjacencyList>> addEdgesL2_List(Broadcast<GrammarChecker> gChecker_broadcast,
			AdjacencyList src_list) {
		List<Tuple2<Integer, AdjacencyList>> list = new ArrayList<Tuple2<Integer, AdjacencyList>>();
		for(byte label = 0; label < this.size_labels; label++){
			AdjacencyList l = src_list.addEdgesL2(gChecker_broadcast, label);
			if(l != null){
				Array a = this.arrays[label];
				for(int index = 0; index < a.getSize(); index++){
					list.add(new Tuple2(a.get(index), l));
				}
			}
		}
		
		return list;
	}

	@Override
	public AdjacencyList addEdgesL1(Map<Byte, Byte> map) {
		Array2DAL nl = new Array2DAL(this.size_labels);
		for(byte olabel: map.keySet()){
			byte nlabel = map.get(olabel);
			nl.finalPut(nlabel, this.arrays[olabel]);
		}
		
		for(byte label = 0; label < nl.size_labels; label++){
			if(nl.arrays[label] == null){
				nl.finalPut(label, EMPTYARRAY);
			}
		}
		
		if(!nl.isEmpty())
			return nl;
		
		return null;
	}

	@Override
	public AdjacencyList merge(AdjacencyList l2) {
		Array2DAL l2_cast = (Array2DAL) l2;
		assert(this.sorted == l2_cast.sorted);
		assert(this.size_labels == l2_cast.size_labels);
		Array2DAL nl = new Array2DAL(this.size_labels, this.sorted);
		
		for(byte label = 0; label < nl.size_labels; label++){
			nl.finalPut(label, this.arrays[label].merge(l2_cast.arrays[label], this.sorted));
		}
		
		return nl;
	}

	@Override
	public AdjacencyList subtract(AdjacencyList l2) {
		// TODO Auto-generated method stub
		Array2DAL l2_cast = (Array2DAL) l2;
		assert(this.size_labels == l2_cast.size_labels);
		Array2DAL nl = new Array2DAL(this.size_labels);
		
		for(byte label = 0; label < nl.size_labels; label++){
			nl.finalPut(label, this.arrays[label].subtract(l2_cast.arrays[label]));
		}
		
		if(!nl.isEmpty()){
			return nl;
		}
		
		return null;
	}

	@Override
	public Iterable<Tuple2<Integer, VLTuple>> flattenDstPairs(int srcId) {
		// TODO Auto-generated method stub
		List<Tuple2<Integer, VLTuple>> list = new ArrayList<Tuple2<Integer, VLTuple>>(this.size_edges);
		for(byte label = 0; label < this.size_labels; label++){
			Array a = this.arrays[label];
			VLTuple vlTuple = new VLTuple(srcId, label); 
			for(int index = 0; index < a.getSize(); index++){
				Tuple2 t = new Tuple2(a.get(index), vlTuple);
				list.add(t);
			}
		}
		
		return list;
	}

	@Override
	public Iterable<Edge> flattenEdges(int srcId) {
		// TODO Auto-generated method stub
		List<Edge> list = new ArrayList<Edge>(this.size_edges);
		for(byte label = 0; label < this.size_labels; label++){
			Array a = this.arrays[label];
			for(int index = 0; index < a.getSize(); index++){
				list.add(new Edge(srcId, a.get(index), label));
			}
		}
		
		return list;
	}
	
	public void finalPut(byte label, Array array){
		assert(this.arrays[label] == null);
		this.arrays[label] = array;
		this.size_edges += array.getSize();
	}


	@Override
	public Iterable<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>> flattenEdgeList(int srcId) {
		List<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>> list = new ArrayList<Tuple2<SrcKey, Tuple2<Byte, AdjacencyList>>>();
		for(byte label = 0; label < this.size_labels; label++){
			Array a = this.arrays[label];
			for(int index = 0; index < a.getSize(); index++){
				list.add(new Tuple2(new SrcKey(srcId, a.get(index)), new Tuple2(label, this)));
			}
		}
		
		return list;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		
		//content
		int n = 0;
		for(byte label = 0; label < this.size_labels; label++){
			Array a = this.arrays[label];
			for(int index = 0; index < a.getSize(); index++){
				builder.append(a.get(index)).append("-").append(label);
				if(++n < this.size_edges){
					builder.append(", ");
				}
			}
		}
		
		//address
		builder.append("]@").append(System.identityHashCode(this));
		
		return builder.toString();
	}

	@Override
	public Iterable<Tuple2<SrcKey, Byte>> flattenSrcEdges(int srcId) {
		List<Tuple2<SrcKey, Byte>> list = new ArrayList<Tuple2<SrcKey, Byte>>();
		for(byte label = 0; label < this.size_labels; label++){
			Array a = this.arrays[label];
			for(int index = 0; index < a.getSize(); index++){
				list.add(new Tuple2(new SrcKey(srcId, a.get(index)), label));
			}
		}
		
		return list;
	}


}
