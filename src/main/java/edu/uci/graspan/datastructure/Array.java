package edu.uci.graspan.datastructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.primitives.Ints;

public class Array {

	private final int[] data;
	
	private int size;
	
	private final int capacity;
	
	
	public Array(int c){
		this.capacity = c;
		this.data = new int[this.capacity];
		this.size = 0;
	}


	public Array(List<Integer> arrayList) {
		this.capacity = arrayList.size();
		this.size = 0;
		this.data = new int[this.capacity];
		for(int i: arrayList){
			add(i);
		}
	}


	public void add(int vId) {
		this.data[this.size++] = vId;
	}

	public int get(int index){
		assert(index < this.size);
		return this.data[index];
	}
	
	public boolean isEmpty(){
		return this.size == 0;
	}
	
	
	public int getSize() {
		return size;
	}


	public int[] getData() {
		return data;
	}


	public int getCapacity() {
		return capacity;
	}


	public Array merge(Array other, boolean sorted) {
		// TODO Auto-generated method stub
		if(this.isEmpty()){
			return other;
		}
		else if(other.isEmpty()){
			return this;
		}
		else{
			Array nArray = new Array(this.size + other.size);
			
			if(sorted){//for srcList
				int i = 0, j = 0;
				while(i < this.size && j < other.size){
					int cmp = this.get(i) - other.get(j);
					if(cmp < 0){
						nArray.add(this.get(i));
						i++;
					}
					else{
						nArray.add(other.get(j));
						j++;
						if(cmp == 0){
							i++;
						}
					}
				}
				if(i < this.size){
					System.arraycopy(this.data, i, nArray.data, nArray.size, this.size - i);
					nArray.size += this.size - i;
				}
				if(j < other.size){
					System.arraycopy(other.data, j, nArray.data, nArray.size, other.size - j);
					nArray.size += other.size - j;
				}
			}
			else{//for dstList whose edges are guaranteed to be distinct 
				System.arraycopy(this.data, 0, nArray.data, 0, this.size);
				nArray.size += this.size;
				
				System.arraycopy(other.data, 0, nArray.data, nArray.size, other.size);
				nArray.size += other.size;
			}
			
			
			return nArray;
		}
	}
	
	public Array subtract(Array other){
		if(this.isEmpty()){
			return this;
		}
		else if(other.isEmpty()){
			return this;
		}
		else{
			Array nArray = new Array(this.size);
			
			int i = 0, j = 0;
			while(i < this.size && j < other.size){
				int cmp = this.get(i) - other.get(j);
				if(cmp < 0){
					nArray.add(this.get(i));
					i++;
				}
				else{
					j++;
					if(cmp == 0){
						i++;
					}
				}
			}
			if(i < this.size){
				System.arraycopy(this.data, i, nArray.data, nArray.size, this.size - i);
				nArray.size += this.size - i;
			}
			
			return nArray;
		}
	}
	
	
	public String toString(){
		if(isEmpty()){
			return "Empty";
		}
		StringBuilder builder = new StringBuilder();
		builder.append("[");
		for(int i = 0; i < this.size - 1; i++){
			builder.append(get(i) + ", ");
		}
		builder.append(get(this.size - 1) + "]");
		return builder.toString();
	}
	

	public static void main(String[] args) {
		int[] a1 = {1, 3, 5, 6, 10};
		int[] a2 = {1, 2, 6, 8, 9};
		Array aa1 = new Array(Ints.asList(a1));
		Array aa2 = new Array(Ints.asList(a2));
		
		System.out.println(aa1.toString());
		System.out.println(aa2.toString());
		System.out.println(aa1.merge(aa2, true));
		System.out.println(aa1.subtract(aa2));
	}
	
	
}
