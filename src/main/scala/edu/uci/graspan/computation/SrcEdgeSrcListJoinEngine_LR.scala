package edu.uci.graspan.computation

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast
import edu.uci.graspan.grammar.GrammarChecker
import edu.uci.graspan.datastructure.Edge
import edu.uci.graspan.datastructure.VLTuple
import org.apache.spark.HashPartitioner
import edu.uci.graspan.datastructure.AdjacencyList
import edu.uci.graspan.datastructure.SrcKey

import scala.collection.JavaConversions._
import collection.JavaConverters._


object SrcEdgeSrcListJoinEngine_LR {
  private var instance: SrcEdgeSrcListJoinEngine_LR = null
  
  def getInstance(m: Byte) = {
    if(instance == null){
      instance = new SrcEdgeSrcListJoinEngine_LR(m)
    }

    instance
  }
}  
  
class SrcEdgeSrcListJoinEngine_LR private (val mode: Byte) extends AbstractEngine {
  
  def run(sc: SparkContext, gChecker_b: Broadcast[GrammarChecker], graph_input: RDD[Edge], number_partitions: Int): RDD[Edge] = {
    println("Calling run at :\t" + this.getClass.getName)
    
    val size_labels = gChecker_b.value.getNumOfGrammarSymbols

    val graph_paired = graph_input.map { e => (e.getSrcId, new VLTuple(e.getDstId, e.getLabel)) }

    val original_list = graph_paired.groupByKey().mapValues { constructEdgeList(_, mode, true, size_labels) }
      .partitionBy(new HashPartitioner(number_partitions))
      .cache()  
    print_rdd_each_partition(original_list, "original_list")
    
    SrcKey.set_num_partitions(number_partitions)
      
    val target_edge_list = original_list.flatMap(flattenTgtEdgeList(_, number_partitions))
      .partitionBy(new HashPartitioner(number_partitions))
      .cache()
    print_rdd_each_partition(target_edge_list, "target_edge_list")

    //-------------------------------------- Iteration 0 ----------------------------------------------
    println("\n-------------- Iteration 0 --------------")  
    
    var src_edges = original_list.flatMap(flattenSrcEdges)
//      .partitionBy(new HashPartitioner(number_partitions))
      .cache()
    print_rdd_each_partition(src_edges, "src_edge_list")
      
    var edges_added_1_list = addEdges1_list(original_list, gChecker_b)
    print_rdd_each_partition(edges_added_1_list, "edges_added_1_list")
    
    var edges_added_2_join = src_edges.join(target_edge_list)
    print_rdd_each_partition(edges_added_2_join, "edges_added_2_join")
    var edges_added_2_list = edges_added_2_join
      .map{ pair => (pair._1.getPartition_id, pair._2._2.addEdgesL2(gChecker_b, pair._2._1)) }.filter(_._2 != null)
      .partitionBy(new HashPartitioner(number_partitions))
      .cache()
    print_rdd_each_partition(edges_added_2_list, "edges_added_2_list")

    var edges_added_list = edges_added_2_list.union(edges_added_1_list).reduceByKey(reduceEdgeList)
      .partitionBy(new HashPartitioner(number_partitions))
      .cache()
    print_rdd_each_partition(edges_added_list, "edges_added_list")
    
    var joined_old_new = original_list.fullOuterJoin(edges_added_list).cache()
    var full_graph = generateFullRDD(joined_old_new).cache()
    print_rdd_each_partition(full_graph, "full_graph")
    var new_edges_list = generateDeltaRDD(joined_old_new).cache()
    print_rdd_each_partition(new_edges_list, "new_edges_list")
    
    //-------------------------------------- Iteration 1 to ? ----------------------------------------------

    var it = 1
    while (!new_edges_list.isEmpty()) {
    	println("\n-------------- Iteration " + it + " --------------")
    	it += 1
      
      src_edges = new_edges_list.flatMap(flattenSrcEdges)
//        .partitionBy(new HashPartitioner(number_partitions))
        .cache()
      print_rdd_each_partition(src_edges, "src_edge_list")

      edges_added_1_list = addEdges1_list(new_edges_list, gChecker_b)
      print_rdd_each_partition(edges_added_1_list, "edges_added_1_list")
      
      var edges_added_2_join = src_edges.join(target_edge_list)
      print_rdd_each_partition(edges_added_2_join, "edges_added_2_join")
      edges_added_2_list = edges_added_2_join
        .map{ pair => (pair._1.getPartition_id, pair._2._2.addEdgesL2(gChecker_b, pair._2._1)) }.filter(_._2 != null)
        .partitionBy(new HashPartitioner(number_partitions))
        .cache()
      print_rdd_each_partition(edges_added_2_list, "edges_added_2_list")
      
      edges_added_list = edges_added_2_list.union(edges_added_1_list).reduceByKey(reduceEdgeList)
        .partitionBy(new HashPartitioner(number_partitions))
        .cache()
      print_rdd_each_partition(edges_added_list, "edges_added_list")
      
      joined_old_new = full_graph.fullOuterJoin(edges_added_list).cache()
      full_graph = generateFullRDD(joined_old_new).cache()
      print_rdd_each_partition(full_graph, "full_graph")
      new_edges_list = generateDeltaRDD(joined_old_new).cache()
      print_rdd_each_partition(new_edges_list, "new_edges_list")
    
    }

    //-------------------------------------- end ----------------------------------------------

    val final_graph = full_graph.flatMap(flattenEdge)

    return final_graph
  }

  def flattenTgtEdgeList(pair: (Int, AdjacencyList), number_partitions: Int): Iterable[(SrcKey, AdjacencyList)] = {
    for(i <- List.range(0, number_partitions, 1); val key = (new SrcKey(i, pair._1)))
      yield  (key, pair._2)
  }

  def flattenSrcEdges(pair: (Int, AdjacencyList)): Iterable[(SrcKey, Byte)] = {
    pair._2.flattenSrcEdges(pair._1).asInstanceOf[java.lang.Iterable[(SrcKey, Byte)]].asScala
  }


}


