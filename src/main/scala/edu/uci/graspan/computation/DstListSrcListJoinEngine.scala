package edu.uci.graspan.computation

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast
import edu.uci.graspan.grammar.GrammarChecker
import edu.uci.graspan.datastructure.Edge
import edu.uci.graspan.datastructure.VLTuple
import org.apache.spark.HashPartitioner
import edu.uci.graspan.datastructure.AdjacencyList


object DstListSrcListJoinEngine {
  private var instance: DstListSrcListJoinEngine = null
  
  def getInstance(m: Byte) = {
    if(instance == null){
      instance = new DstListSrcListJoinEngine(m)
    }

    instance
  }
}  
  
class DstListSrcListJoinEngine private (val mode: Byte) extends AbstractEngine {
  
  def run(sc: SparkContext, gChecker_b: Broadcast[GrammarChecker], graph_input: RDD[Edge], number_partitions: Int): RDD[Edge] = {
    println("Calling run at :\t" + this.getClass.getName)
    
    val size_labels = gChecker_b.value.getNumOfGrammarSymbols

    val graph_paired = graph_input.map { e => (e.getSrcId, new VLTuple(e.getDstId, e.getLabel)) }

    val original_list = graph_paired.groupByKey().mapValues { constructEdgeList(_, mode, true, size_labels) }
      .partitionBy(new HashPartitioner(number_partitions)).cache()

    //-------------------------------------- Iteration 0 ----------------------------------------------
    println("\n-------------- Iteration 0 --------------")  

    var old_dst_list = original_list.flatMap(flattenDstPair).groupByKey().mapValues { constructEdgeList(_, mode, false, size_labels) }
      .partitionBy(new HashPartitioner(number_partitions)).cache()

    val edges_added_1_list = addEdges1_list(original_list, gChecker_b)
    val edges_added_2_list = addEdges2_list_ll(old_dst_list, original_list, gChecker_b)

    val edges_added_list = edges_added_2_list.union(edges_added_1_list).reduceByKey(reduceEdgeList)

    val joined_old_new = original_list.fullOuterJoin(edges_added_list).cache()
    var full_graph = generateFullRDD(joined_old_new).cache()
    var new_edges_list = generateDeltaRDD(joined_old_new).cache()

    //-------------------------------------- Iteration 1 to ? ----------------------------------------------

    var it = 1
    while (!new_edges_list.isEmpty()) {
      println("\n-------------- Iteration " + it + " --------------")
    	it += 1
    	
      val new_dst_list = new_edges_list.flatMap(flattenDstPair).groupByKey().mapValues { constructEdgeList(_, mode, false, size_labels) }
        .partitionBy(new HashPartitioner(number_partitions)).cache()

      val new_edges_added_1_list = addEdges1_list(new_edges_list, gChecker_b)
      val new_edges_added_2_list_on = addEdges2_list_ll(old_dst_list, new_edges_list, gChecker_b)
      val new_edges_added_2_list_nf = addEdges2_list_ll(new_dst_list, full_graph, gChecker_b)

      val new_edges_added_list = new_edges_added_2_list_nf.union(new_edges_added_2_list_on).union(new_edges_added_1_list).reduceByKey(reduceEdgeList)

      val joined_old_new = full_graph.fullOuterJoin(new_edges_added_list).cache()
      full_graph = generateFullRDD(joined_old_new).cache()
      new_edges_list = generateDeltaRDD(joined_old_new).cache()

      old_dst_list = old_dst_list.union(new_dst_list)
        .reduceByKey(reduceEdgeList)

    }

    //-------------------------------------- end ----------------------------------------------

    val final_graph = full_graph.flatMap(flattenEdge)

    return final_graph
  }


}


