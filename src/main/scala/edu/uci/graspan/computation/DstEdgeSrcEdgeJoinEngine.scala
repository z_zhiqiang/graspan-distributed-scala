package edu.uci.graspan.computation

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.broadcast.Broadcast
import edu.uci.graspan.datastructure.Edge
import edu.uci.graspan.grammar.GrammarChecker
import org.apache.spark.HashPartitioner
import org.apache.spark.RangePartitioner

object DstEdgeSrcEdgeJoinEngine {
  private var instance : DstEdgeSrcEdgeJoinEngine = null
  
  def getInstance() = {
    if(instance == null)
      instance = new DstEdgeSrcEdgeJoinEngine()
    instance
  }
}

class DstEdgeSrcEdgeJoinEngine private extends AbstractEngine {
  def run(sc: SparkContext, gChecker_b: Broadcast[GrammarChecker], graph_input: RDD[Edge], number_partitions: Int): RDD[Edge] = {

    var graph_paired = graph_input.map { e => (e.getSrcId, (e.getDstId, e.getLabel)) }
      .partitionBy(new HashPartitioner(number_partitions)).cache()
//    graph_paired = graph_paired.partitionBy(new RangePartitioner(number_partitions, graph_paired)).cache()

    println("\n-------------- Iteration 0 --------------")  

    var old_dst_paired = createDstRDD(graph_paired)
      .partitionBy(new HashPartitioner(number_partitions)).cache()
//    old_dst_paired = old_dst_paired.partitionBy(new RangePartitioner(number_partitions, old_dst_paired)).cache()

    val edges_added_1 = addEdges1(gChecker_b, graph_paired)
    val edges_added_2 = addEdges2(gChecker_b, old_dst_paired, graph_paired)

    var new_paired: RDD[(Int, (Int, Byte))] = edges_added_2.union(edges_added_1).distinct().subtract(graph_paired)
      .partitionBy(new HashPartitioner(number_partitions)).cache()
//    new_paired = new_paired.partitionBy(new RangePartitioner(number_partitions, new_paired)).cache()

    var full_paired = graph_paired.union(new_paired)
      .cache()
//    full_paired = full_paired.partitionBy(new RangePartitioner(number_partitions, full_paired)).cache()

    var it = 1
    while (!new_paired.isEmpty()) {
      println("\n-------------- Iteration " + it + " --------------")
    	it += 1
    	
      var new_dst_paired = createDstRDD(new_paired)
        .partitionBy(new HashPartitioner(number_partitions)).cache()
//      new_dst_paired = new_dst_paired.partitionBy(new RangePartitioner(number_partitions, new_dst_paired)).cache()

      val new_edges_added_1 = addEdges1(gChecker_b, new_paired)
      val new_edges_added_2_on = addEdges2(gChecker_b, old_dst_paired, new_paired)
      val new_edges_added_2_nf = addEdges2(gChecker_b, new_dst_paired, full_paired)

      old_dst_paired = old_dst_paired.union(new_dst_paired)
//      old_dst_paired = old_dst_paired.partitionBy(new RangePartitioner(number_partitions, old_dst_paired)).cache()

      new_paired = new_edges_added_2_on.union(new_edges_added_2_nf).union(new_edges_added_1).distinct().subtract(full_paired)
        .partitionBy(new HashPartitioner(number_partitions)).cache()
//      new_paired = new_paired.partitionBy(new RangePartitioner(number_partitions, new_paired)).cache()

      full_paired = full_paired.union(new_paired)
        .cache()
//      full_paired = full_paired.partitionBy(new RangePartitioner(number_partitions, full_paired)).cache()

    }

    val final_graph = full_paired.map { t => new Edge(t._1, t._2._1, t._2._2) }

    return final_graph
  }

  def createDstRDD(graph_src_paired: RDD[(Int, (Int, Byte))]): RDD[(Int, (Int, Byte))] = {
    val graph_dst_paired = graph_src_paired.map { t =>
      (t._2._1, (t._1, t._2._2))
    }
    return graph_dst_paired
  }

  def addEdges1(gChecker_b: Broadcast[GrammarChecker], graph_src_paired: RDD[(Int, (Int, Byte))]): RDD[(Int, (Int, Byte))] = {
    val edges_added_1 = graph_src_paired.mapValues { t =>
      val rLabel = gChecker_b.value.checkL1Rules(t._2)
      if (rLabel != -1) (t._1, rLabel) else null
    }.filter(e => e._2 != null)

    return edges_added_1
  }

  def addEdges2(gChecker_b: Broadcast[GrammarChecker], graph_dst_paired: RDD[(Int, (Int, Byte))], graph_src_paired: RDD[(Int, (Int, Byte))]): RDD[(Int, (Int, Byte))] = {
    val new_edges_added_2 = graph_dst_paired.join(graph_src_paired).values.map { p =>
      val rLabel = gChecker_b.value.checkL2Rules(p._1._2, p._2._2)
      if (rLabel != -1) (p._1._1, (p._2._1, rLabel)) else null
    }.filter(e => e != null)

    return new_edges_added_2
  }

}