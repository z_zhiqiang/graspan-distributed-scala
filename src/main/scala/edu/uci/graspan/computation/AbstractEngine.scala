package edu.uci.graspan.computation

import org.apache.spark.rdd.RDD
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.SparkContext
import edu.uci.graspan.datastructure.Edge
import edu.uci.graspan.grammar.GrammarChecker
import edu.uci.graspan.datastructure.AdjacencyList
import edu.uci.graspan.datastructure.VLTuple
import edu.uci.graspan.datastructure.HashSetAL

import collection.JavaConverters._
import org.apache.spark.api.java.JavaPairRDD
import com.google.common.base.Optional
import edu.uci.graspan.computation.AbstractEngineJava

trait AbstractEngine extends Serializable {
  def run(sc: SparkContext, gChecker_b: Broadcast[GrammarChecker], graph_input: RDD[Edge], number_partitions: Int): RDD[Edge]

  protected def constructEdgeList(list: Iterable[VLTuple], mode: Byte, sorted_flag: Boolean, size_labels: Byte): AdjacencyList = {
    return new AbstractEngineJava.EdgeListConstructor(mode, sorted_flag, size_labels).call(list.asJava)
  }

  protected def flattenDstPair(t: (Int, AdjacencyList)): Iterable[(Int, VLTuple)] = {
    return new AbstractEngineJava.DstPairFlattener().call(t.asInstanceOf[(Integer, AdjacencyList)]).asInstanceOf[java.lang.Iterable[(Int, VLTuple)]].asScala
  }

  protected def addEdges1_list(edge_list: RDD[(Int, AdjacencyList)], gChecker_b: Broadcast[GrammarChecker]): RDD[(Int, AdjacencyList)] = {
    //    return AbstractEngineJava.addEdges1_list(edge_list.asInstanceOf[JavaPairRDD[Integer, AdjacencyList]], gChecker_b).asInstanceOf[RDD[(Int, AdjacencyList)]]
    return edge_list.mapValues { _.addEdgesL1(gChecker_b) }.filter(_._2 != null)
  }

  protected def reduceEdgeList(l1: AdjacencyList, l2: AdjacencyList): AdjacencyList = {
    return new AbstractEngineJava.EdgeListReducer().call(l1, l2)
  }

  protected def flattenEdge(t: (Int, AdjacencyList)): Iterable[Edge] = {
    return new AbstractEngineJava.EdgeFlattener().call(t.asInstanceOf[(Integer, AdjacencyList)]).asScala
  }

  protected def generateFullRDD(joined_old_new: RDD[(Int, (Option[AdjacencyList], Option[AdjacencyList]))]): RDD[(Int, AdjacencyList)] = {
    //    return AbstractEngineJava.generateFullRDD(joined_old_new.asInstanceOf[JavaPairRDD[Integer, (Optional[AdjacencyList], Optional[AdjacencyList])]]).asInstanceOf[RDD[(Int, AdjacencyList)]]

    return joined_old_new.mapValues { t =>
      if (t._1.isEmpty)
        t._2.get
      else if (t._2.isEmpty)
        t._1.get
      else
        t._1.get.merge(t._2.get)
    }
  }

  protected def generateDeltaRDD(joined_old_new: RDD[(Int, (Option[AdjacencyList], Option[AdjacencyList]))]): RDD[(Int, AdjacencyList)] = {
    //    return AbstractEngineJava.generateDeltaRDD(joined_old_new.asInstanceOf[JavaPairRDD[Integer, (Optional[AdjacencyList], Optional[AdjacencyList])]]).asInstanceOf[RDD[(Int, AdjacencyList)]]

    return joined_old_new.mapValues { t =>
      if (t._1.isEmpty)
        t._2.get
      else if (t._2.isEmpty)
        null
      else
        t._2.get.subtract(t._1.get)
    }.filter(_._2 != null)
  }

  protected def addEdges2_list_el(dst_paired: RDD[(Int, VLTuple)], edges_list: RDD[(Int, AdjacencyList)], gChecker_b: Broadcast[GrammarChecker]): RDD[(Int, AdjacencyList)] = {
    //    return AbstractEngineJava.addEdges2_list_el(dst_paired.asInstanceOf[JavaPairRDD[Integer, VLTuple]], edges_list.asInstanceOf[JavaPairRDD[Integer, AdjacencyList]], gChecker_b).asInstanceOf[RDD[(Int, AdjacencyList)]]

    return dst_paired.join(edges_list).values.map { t => val list = t._2.addEdgesL2(gChecker_b, t._1.getLabel); if (list != null) (t._1.getVId, list) else null }
      .filter(_ != null).reduceByKey(reduceEdgeList(_, _))
  }

  protected def addEdges2_list_ll(dst_list: RDD[(Int, AdjacencyList)], src_list: RDD[(Int, AdjacencyList)], gChecker_b: Broadcast[GrammarChecker]): RDD[(Int, AdjacencyList)] = {
    //    return AbstractEngineJava.addEdges2_list_ll(dst_list.asInstanceOf[JavaPairRDD[Integer, AdjacencyList]], src_list.asInstanceOf[JavaPairRDD[Integer, AdjacencyList]], gChecker_b).asInstanceOf[RDD[(Int, AdjacencyList)]]

    return dst_list.join(src_list).values.flatMap { t => t._1.addEdgesL2_List(gChecker_b, t._2).asInstanceOf[java.lang.Iterable[(Int, AdjacencyList)]].asScala }
      .filter(_ != null).reduceByKey(reduceEdgeList(_, _))
  }

  
  protected def print_rdd_each_partition(rdd: org.apache.spark.rdd.RDD[_], name: String){
//    println(name)
//    val prdd = rdd.glom().collect()
//    var i = 0
//    while(i < prdd.length){
//      println(i + ":")
//      prdd(i).foreach { t => println(t.toString()) }
//      i = i + 1
//    }
//    println()
  }
  
  protected def print_rdd_each_partition_console(rdd: org.apache.spark.rdd.RDD[_]){
    rdd.mapPartitionsWithIndex{(index, It) => {It.map(println)}}
  }
  
  
}