package edu.uci.graspan

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.log4j.Logger
import org.apache.log4j.Level
import java.io.File
import edu.uci.graspan.grammar.GrammarChecker
import edu.uci.graspan.preprocess.Preprocessor
import edu.uci.graspan.computation.AbstractEngine
import edu.uci.graspan.computation.DstEdgeSrcEdgeJoinEngine
import java.text.SimpleDateFormat
import edu.uci.graspan.grammar.GrammarChecker
import edu.uci.util.FileUtil
import edu.uci.graspan.computation.DstEdgeSrcListJoinEngine
import edu.uci.graspan.computation.DstListSrcListJoinEngine
import edu.uci.graspan.computation.SrcListSrcListJoinEngine_LR
import edu.uci.graspan.computation.AbstractEngineJava
import edu.uci.graspan.computation.SrcEdgeSrcListJoinEngine_LR

object Graspan {
  
  def main(args: Array[String]) {
    if(args.length != 4){
      println("Usage: input_graph_file input_grammar_file number_partitions compute_mode");
      System.exit(0);
    }
    
    //inputs
    val input_graph_file = args(0);
    val input_grammar_file = args(1);
    val number_partitions = args(2).toInt;
    val mode = args(3).toByte;
    
    //set logger
    Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		
    //initiate Spark context
		val sc = initiateSparkContext();
		
		//read grammar
		val gChecker = new GrammarChecker(new File(input_grammar_file));
		//get the broadcasted grammars
		val gChecker_broadcast = sc.broadcast(gChecker)
		
		val prestime = System.currentTimeMillis()
		//pre-processing
		val input_graph = Preprocessor.preprocess(sc, gChecker_broadcast, input_graph_file, number_partitions).cache()
		val preetime = System.currentTimeMillis()
		val input_count = input_graph.count();
		println(input_count)
    
		val cstime = System.currentTimeMillis()
		//core computation
		val final_graph = getEngine(mode).run(sc, gChecker_broadcast, input_graph, number_partitions).cache()
		val cetime = System.currentTimeMillis()
		val final_count = final_graph.count();
		
		val poststime = System.currentTimeMillis()
		//post-processing
		val postetime = System.currentTimeMillis()
    
		//terminate 
		sc.stop()
		
		//print out results to console
		println();
		println("------------------------------------------");
		println("Number of edges originally:\t" + input_count);
		println("Number of edges added:     \t" + (final_count - input_count));
		println("Number of edges finally:   \t"+ final_count);
		println("------------------------------------------");
		val preTime = (preetime - prestime) / 1000;
		val compTime = (cetime - cstime) / 1000;
		val postTime = (postetime - poststime) / 1000;
		val totalTime = preTime + compTime + postTime;
		println("\nPreprocess time cost:    \t" + preTime);
		println("\nComputation time cost:   \t" + compTime);
		println("\nPostprocess time cost:   \t" + postTime);
		println("\nTotal time cost:         \t" + totalTime);
		println("------------------------------------------");
		
		val timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH:mm:ss").format(new java.util.Date());
		val input_graph_name = new File(args(0)).getName();
		val input_grammar_name = new File(args(1)).getName();
		FileUtil.appendToFile("console.txt", timeStamp + ", " 
				+ input_graph_name + ", " + input_grammar_name + ", " 
				+ number_partitions + ", " + mode + ", " 
				+ input_count + ", " + final_count + ", "
				+ preTime + ", " + compTime + ", " + postTime + ", " + totalTime);
		
  }
  

  def initiateSparkContext(): SparkContext = {
    val conf = new SparkConf().setAppName("Distributed Graspan").setMaster("local[4]")
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
		  .set("spark.kryoserializer.buffer.mb","24") 
		  .set("spark.cores.max", "1")
		  ;
    return new SparkContext(conf);
  }

  def getEngine(mode: Byte): AbstractEngine = {
    mode match {
      //edge-edge join
      case 0 => DstEdgeSrcEdgeJoinEngine.getInstance()
      
      //edge-srcList join via HashSet
      case 3 => DstEdgeSrcListJoinEngine.getInstance(AbstractEngineJava.HASHSETAL)
      
      //edge-srcList join via SortedArray
      case 4 => DstEdgeSrcListJoinEngine.getInstance(AbstractEngineJava.SORTARRAYAL)
      
      //edge-srcList join via Array2D
      case 7 => DstEdgeSrcListJoinEngine.getInstance(AbstractEngineJava.ARRAY2DAL)

      //dstList-srcList join via HashSet
      case 5 => DstListSrcListJoinEngine.getInstance(AbstractEngineJava.HASHSETAL)
      
      //dstList-srcList join via SortedArray
      case 6 => DstListSrcListJoinEngine.getInstance(AbstractEngineJava.SORTARRAYAL)
      
      //dstList-srcList join via Array2D
      case 8 => DstListSrcListJoinEngine.getInstance(AbstractEngineJava.ARRAY2DAL)
      
      //srcList-srcList join for left regular grammars via HashSet
      case 9 => SrcListSrcListJoinEngine_LR.getInstance(AbstractEngineJava.HASHSETAL)
      
      //srcList-srcList join for left regular grammars via SortedArray
      case 10 => SrcListSrcListJoinEngine_LR.getInstance(AbstractEngineJava.SORTARRAYAL)
      
      //srcList-srcList join for left regular grammars via Array2D
      case 11 => SrcListSrcListJoinEngine_LR.getInstance(AbstractEngineJava.ARRAY2DAL)
      
      //edge-srcList join for left regular grammars via HashSet
      case 12 => SrcEdgeSrcListJoinEngine_LR.getInstance(AbstractEngineJava.HASHSETAL)
      
      //edge-srcList join for left regular grammars via SortedArray
      case 13 => SrcEdgeSrcListJoinEngine_LR.getInstance(AbstractEngineJava.SORTARRAYAL)
      
      //edge-srcList join for left regular grammars via Array2D
      case 14 => SrcEdgeSrcListJoinEngine_LR.getInstance(AbstractEngineJava.ARRAY2DAL)
      
      case _ => throw new RuntimeException("Wrong mode!!!")
    }
  }
  
}